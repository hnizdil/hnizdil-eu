# Init

- Install hugo: `brew install hugo`
- Create a site: `hugo new site blog`
- Initialize as a Hugo module: `hugo mod init gitlab.com/hnizdil/hnizdil-eu`
- Follow installation instructions on https://github.com/schnerring/hugo-theme-gruvbox

# Create a New Post

```shell
hugo new content blog/post-title/index.md
```