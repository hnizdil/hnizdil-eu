module gitlab.com/hnizdil/hnizdil-eu

go 1.22.1

require (
	github.com/schnerring/hugo-mod-json-resume v0.0.0-20240317040430-885b96a21f48 // indirect
	github.com/schnerring/hugo-theme-gruvbox v0.0.0-20240315014043-dc3c0a888556 // indirect
)
